image: python:3.8

stages:
  - lint
  - test
  #- quality
  - build
  - package 
  - deploy
  - release

include:
- template: Security/Dependency-Scanning.gitlab-ci.yml
- template: Security/SAST.gitlab-ci.yml
- template: Security/Container-Scanning.gitlab-ci.yml
- template: Security/Secret-Detection.gitlab-ci.yml #Secret-Detection.latest.gitlab-ci.yml

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  SONAR_TOKEN: "XXXXX"  # token for sonarqube server
  SONAR_URL: "XXXXX"    # address for sonarqube
  AGENT_CODE: "XXXX"    # input agent code here ater registration 


cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python --version ; pip --version  # For debugging
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate

lint_job:
  stage: lint
  script:
    - pip install -r requirements.txt
    - pylint src/
  artifacts:
    paths:
      - reports/pylint-report.txt

.sonarqube:
  stage: quality
  image: sonarsource/sonar-scanner-cli:latest
  script:
    - sonar-scanner -Dsonar.projectKey=my-project -Dsonar.sources=src/ -Dsonar.host.url=https://$SONAR_URL -Dsonar.login=$SONAR_TOKEN

test_job:
  stage: test
  script:
    - pip install ruff tox  # you can also use tox
    - pip install --editable ".[test]"
    - tox -e py,ruff
    - pip install -r requirements.txt
    - pytest tests/
  artifacts:
    paths:
      - reports/junit.xml

build_job:
  stage: build
  script:
    - pip install .      # run the command here
  artifacts:
    paths:
      - build/*

secret_detection:
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"

package_image:
  services:
  - name: docker:dind
    alias: dind
  image: docker:20.10.16
  stage: package
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --tag $CONTAINER_TEST_IMAGE --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CONTAINER_TEST_IMAGE
    - docker push $CI_REGISTRY_IMAGE:latest
  needs: [build_job]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

container_scanning:
  stage: package
  variables: 
    CS_IMAGE: $CI_REGISTRY_IMAGE:latest
    SECURE_LOG_LEVEL: 'debug'
  needs: [package_image]

deploy_job:
  stage: deploy
  image:
    name: google/cloud-sdk
    entrypoint: ['']
  variables:
    KUBE_CONTEXT: my-context 
    AGENT_ID: $AGENT_CODE 
    K8S_PROXY_URL: https://kas.gitlab.com/k8s-proxy/ 
  before_script:
    - kubectl config set-credentials agent:$AGENT_ID --token="ci:${AGENT_ID}:${CI_JOB_TOKEN}"
    - kubectl config set-cluster gitlab --server="${K8S_PROXY_URL}"
    - kubectl config set-context "$KUBE_CONTEXT" --cluster=gitlab --user="agent:${AGENT_ID}"
    - kubectl config use-context "$KUBE_CONTEXT"
    - apt-get update && apt-get install -y gettext
  script:
    - kubectl get nodes
    - kubectl get pods
    - GITLAB_TOKEN=$(echo "${CI_JOB_TOKEN}" | base64)
    - kubectl delete secret gitlab-creds
    - kubectl create secret generic gitlab-creds --from-literal=token=${GITLAB_TOKEN}
    - envsubst < manifest/manifest.yml > deployement.yml
    - kubectl apply -f deployement.yml
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Run this job when commits are pushed or merged to the default branch
      when: manual                          
  script:
    - echo "running release_job for $TAG"
  release:                                         # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: 'v0.$CI_PIPELINE_IID'                # The version is incremented per pipeline.
    description: 'v0.$CI_PIPELINE_IID'
    ref: '$CI_COMMIT_SHA'                          # The tag is created from the pipeline SHA.
