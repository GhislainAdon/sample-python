
# Définit l'image de base
FROM python:3.8-slim-buster

LABEL NAME="python-3.8" \
      VERSION="3.8" \
      DESC="Python3.8 container"

# Met à jour les paquets et installe les dépendances du système
RUN apt-get update && \
    apt-get install -y build-essential && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Crée un utilisateur non-root pour exécuter l'application
RUN useradd --create-home --shell /bin/bash appuser
WORKDIR /home/appuser
USER appuser

# Installe les dépendances Python
COPY requirements.txt .
RUN pip install --user -r requirements.txt

# Copie le code source dans le conteneur
COPY . .

# Expose le port utilisé par l'application
EXPOSE 8000

# Lance l'application
CMD ["python", "app.py"]
